from gpiozero import OutputDevice
from threading import Thread

import time
import atexit

import requests
import json

PI_SERVER_IP = "localhost"
PI_SERVER_PORT = 8001
PI_JSON_LINK = str(PI_SERVER_IP) + ":" + str(PI_SERVER_PORT) + "/room_info"

HUMIDITY_MINIMUM = 55

POWER_ON_TIME =  20
DATA_PIN = 26

def power_on(out, time_sleep=0):
    """
    Used to close normally open circuit on relay
    """
    out.off()
    time.sleep(time_sleep)

def power_off(out, time_sleep=0):
    """
    Used to open a normally open circuit
    """
    out.on()
    time.sleep(time_sleep)


"""
Shut off humidifier on exit
"""
atexit.register(power_off)

def main():
    out = OutputDevice(DATA_PIN, initial_value=True)

    room_humidity = HUMIDITY_MINIMUM + 1

    try:
        room_info_response = requests.get("http://" + str(PI_JSON_LINK))
        room_humidity = float(room_info_response.json()['room_info']['humidity'])
    except Exception as e:
        print('Could not get room humidity\n' + str(e))
        room_humidity = HUMIDITY_MINIMUM + 1

    while True:
        try:
            room_info_response = requests.get("http://" + str(PI_JSON_LINK))
            room_humidity = float(room_info_response.json()['room_info']['humidity'])
        except Exception as e:
            print('Could not get room humidity\n' + str(e))
            room_humidity = HUMIDITY_MINIMUM + 1

        if room_humidity < HUMIDITY_MINIMUM:
            thread = Thread(target=power_on, args=[out], kwargs={'time_sleep' : POWER_ON_TIME})
            thread.start()
            thread.join()
        else:
            power_off(out, time_sleep=POWER_ON_TIME)

if __name__ == '__main__':
    # main function ran in thread to avoid locking the main thread
    # as pizero only has 1 core
    print("Power on time: " + str(POWER_ON_TIME / 60)  + " minutes")
    print("Humidity minimum: " + str(HUMIDITY_MINIMUM) + "%")
    thread = Thread(target=main)
    thread.start()

