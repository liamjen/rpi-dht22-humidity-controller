from json import dumps
from flask import Flask, request
from flask_restful import Resource, Api
import socket

from threading import Thread

from DHT22 import DHT22_Timer
# from room_info_database import ClimateDBConn

DHT22_GPIO_PIN = 2
DHT22_UPDATE_INTERVAL = 20

DATABASE_UPDATE_LIMIT = 10 * 60

def main():
    # db_connection = ClimateDBConn('room_info', 'climate', insert_limit_seconds=DATABASE_UPDATE_LIMIT)
    dht22_timer = DHT22_Timer(DHT22_GPIO_PIN, DHT22_UPDATE_INTERVAL)
    dht22_update_thread = Thread(target=dht22_timer.start_loop, kwargs={})

    #'update_callback':lambda: db_connection.insert(dht22_timer.last_temperature, dht22_timer.last_humidity)

    dht22_update_thread.start()

    app = Flask(__name__)
    api = Api(app)

    class RoomInfo(Resource):
        def get(self):
            return {'room_info' :
                            {
                                 "humidity"    : round(dht22_timer.last_humidity, 2),
                                 "temperature" : round(dht22_timer.last_temperature, 2)
                            }
                   }

    api.add_resource(RoomInfo, '/room_info')

    app.run(port='8001', host='0.0.0.0')

if __name__ == '__main__':
    thread = Thread(target=main)
    thread.start()
