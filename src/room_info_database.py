from pymongo import MongoClient
import datetime, time

class ClimateDBConn:
    def __init__(self, db_name, collection_name, insert_limit_seconds=0):
        """
        insert_limit_seconds = impose a limit on how often data can be inserted
        """
        self._client = MongoClient() # Connects to default host and port
        self._db = self._client[db_name]
        self._collection = self._db[collection_name]
        self.insert_limit_seconds = insert_limit_seconds
        self._last_insert_time = 0

    def insert(self, temp, humidity):
        current_time = time.time()
        if current_time - self._last_insert_time > self.insert_limit_seconds:
            document = {
                           'time'        : datetime.datetime.now(),
                           'temperature' : temp,
                           'humidity'    : humidity
                       }
            self._collection.insert_one(document)
            self._last_insert_time = current_time

    def get_last_update(self):
        last_element = self._collection.find(limit=1, sort=[('$natural', -1)])[0]
        return {
                'temperature' : last_element['temperature'],
                'humidity'    : last_element['humidity']
               }


